var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

module.exports = {
    context: __dirname,
    devServer: {
        // This is required for older versions of webpack-dev-server
        // if you use absolute 'to' paths. The path should be an
        // absolute path to your build destination.
        outputPath: path.join(__dirname, './wwwroot/dist')
        
    },
    plugins: [
        new CopyWebpackPlugin([
            // {output}/file.txt
            { from: 'node_modules/chart.js' }

            
        ], {
            ignore: []
        })
    ]
};