/**
 * {
      "jobsiteId": 2,
      "jobsiteCode": "0065006470",
      "jobsiteDesc": "BALCONES DE MORELOS SERVER II",
      "latitude": 25.675827,
      "longitude": -100.416799,
      "favoriteStatus": "Y"
    }
 */

import AddressDTO from './addressDTO'
import OrderDTO from './orderDTO'

export default class JobsiteDTO {
    // From jobsites api call
    jobsiteId: number;
    jobsiteCode: string
    jobsiteDesc: string;
    latitude: number;
    longitude: number;
    favoriteStatus: string;

    // From summary api call
    measureUnit: string;
    orders: OrderDTO[];

    // From customers api call
    address: AddressDTO;
}