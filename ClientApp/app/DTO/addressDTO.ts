/**
 * {
        "addressId": 466,
        "cityDesc": "SALTILLO",
        "regionId": 7,
        "regionDesc": "Coahuila",
        "countryCode": "MX ",
        "countryDesc": "Mexico"
    }
 */

export default class AddressDTO {
    addressId: number;
    cityDesc: string;
    regionId: number;
    regionDesc: string;
    countryCode: string;
    countryDesc: string;
}