/*
----Sample data
applications: [] (0)
countryCode: "0"
customer: {customerId: 1511, customerName: "CASAS YES S.A. DE C.V."}
email: "f-vpaz"
firstName: "Vicente"
fullName: "Vicente Paz"
jobsites: [Object] (1)
lastName: "Paz"
phone: "8122334455"
powerUser: "S"
regions: [] (0)
status: "A"
userId: 152


*/
import CustomerDTO from './customerDTO';
import AddressDTO from './addressDTO';
import JobsiteDTO from './jobsiteDTO';

export class UserDTO{
    countryCode:string;
    customer:CustomerDTO;
    email:string;
    firstName:string;
    fullName:string;
    jobsites:JobsiteDTO[];
    lastName:string;
    phone:string;
    powerUser:string;
    regions:any;
    status:string;
    userId:number;
}