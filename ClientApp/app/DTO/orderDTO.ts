export default class orderDTO {
    orderId: number;
    orderStatusGroupCode: string;
    orderCode: string;
    orderStatusDesc: string;
    shippingConditionId: number;
    instructionsDesc: string;
    purchaseOrder: string;

    // From jobsite api
    totalLoads: number;
    totalDeliveries: number;
    totalQuantity: number;
    deliveredQuantity: number;
    programedDateTime: Date;
    orderStatus: string;
    isReadyMix: boolean;
}