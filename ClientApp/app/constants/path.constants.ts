import {CredentialsConstants} from './credentials.constants';
export class PathConstants {


    static workingPath="";
    static workingEnv="development";
    
    //base paths to start the call
    static basePath="https://api.us.apiconnect.ibmcloud.com/";
    static basePathDev="http://other/path/";
    
    //part of the middle of the URL
    static prodURL="cnx-gbl-org-production/production/v1/";
    static qaURL="cnx-gbl-org-quality/qa/v1/";
    static devURL="cnx-gbl-org-quality/qa/v1/";

    //real paths
    static LOGIN_PATH = "secm/oam/oauth2/token";
    static USER_CONFIG = "secm/customers";
    static USERS_PATH = "secm/users";
    static USER_PATH = "secm/users/";
    static RECOVER_PATH = "secm/recoverpwd";
    static USER_INFO= "secm/myinfo";
    static REQUEST_HISTORY_PATH = "dm/orderrequest/plants?";
    static REQUEST_PATH = "dm/orderrequest";
    static PlANTS_PATH = "dm/plants";
    static BRIEF_PLANTS_PATH = "dm/orderrequest/group"
    static PLANT_PATH = "dm/plants/plant";
    static UPDATE_REQUEST_PATH = "dm/orderrequest/request";

    static getWorkingPath():string{
       if(this.basePathDev=="http://other/path/"){
           //development environment
           this.workingPath=this.basePath;
       } else {
           this.workingPath=this.basePathDev;
       }
       switch(this.workingEnv){
           case "development":
           this.workingPath=this.workingPath+this.devURL;
           break;
           case "quality":
           this.workingPath=this.workingPath+this.qaURL;
           break;
           case "production":
           this.workingPath=this.workingPath+this.prodURL;
           break;
       }
        return this.workingPath;
    }

    /**
     * Possible values for environment
     * development
     * quality
     * production
     */
   static setEnv(env:string):void{
       if(env=="development"){
           this.workingEnv="development";
       } else if(env=="quality"){
           this.workingEnv="quality";
       } else if(env=="production"){
           this.workingEnv="production";
       }
       CredentialsConstants.setEnv(this.workingEnv);
   }
}