export class CredentialsConstants{
    static workingEnv="development";
    static workingKey=""
    static workingAppCode=""
    static workingJwt=""
    static workingBearer=""

    static keyDev = "b6436ca9-ac0d-4bb6-9bdb-5884554d5183"
    static keyQa = "b6436ca9-ac0d-4bb6-9bdb-5884554d5183"
    static keyProd = "0f76055f-11b6-48b4-997a-f8f4897d3bf9"
        
    static setAppCode(appCode:string):void{
        this.workingAppCode=appCode;
    }
    static getBearer():string{
        return this.workingBearer;
    }
    static setBearer(value:string){
        this.workingBearer=value;
    }
    static setKeys(dev:string,qa:string,prod:string){
        this.keyDev=dev;
        this.keyQa=qa;
        this.keyProd=prod;
    }
    static getAppCode():string{
        return this.workingAppCode;
    }

    static setJWT(jwt:string):void{
        this.workingJwt=jwt;
    }
    static getJWT():string{
        return this.workingJwt;
    }

    static getKey():string{
        if(this.workingEnv=="development"){
            return this.keyDev;
        } else if(this.workingEnv=="quality"){
            return this.keyQa;
        } else if(this.workingEnv=="production"){
            return this.keyProd;
        }
    }
    
    static setEnv(env:string):void{
       if(env=="development"){
           this.workingEnv="development";
           CredentialsConstants
       } else if(env=="quality"){
           this.workingEnv="quality";
       } else if(env=="production"){
           this.workingEnv="production";
       }
   }
}