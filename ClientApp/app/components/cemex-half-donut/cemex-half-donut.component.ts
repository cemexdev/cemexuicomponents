import { Component } from '@angular/core';
import {Logger} from '../../services/logger.service';
// webpack html imports
let template = require('./cemex-half-donut.component.html');

@Component({
  selector: 'cemex-half-donut',
  template: template,
  styleUrls: ['./cemex-half-donut.component.scss']
})
export class CemexHalfDonutComponent {
  // Doughnut
  public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';
  private doughnutChartColors: any[] = [{ backgroundColor: ["#b8436d", "#00d9f9", "#a4c73c", "#a4add3"] }];
  private chartOptions:any={rotation: 1 * Math.PI,circumference: 1 * Math.PI};

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
  private getLabelColor(index:number):string{
    Logger.log(this.doughnutChartColors[0].backgroundColor[index]);
    return this.doughnutChartColors[0].backgroundColor[index];
  }
}