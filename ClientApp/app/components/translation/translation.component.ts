import { Component } from '@angular/core';
import {SessionService} from '../../services/session.service';
import {HttpCemex} from '../../services/http.service';
import {CredentialsConstants} from '../../constants/credentials.constants';
import {Broadcaster} from '../../events/broadcaster.event';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';

@Component({
    selector: 'translationExample',
    templateUrl: './translation.component.html',
    providers: [SessionService,HttpCemex,Broadcaster,TranslationService]
})
export class TranslationComponent {

    private translations:Map<string,string>=new Map();
    private texto:string;
    
    constructor(private loginService:SessionService,private eventDispatcher:Broadcaster,private t:TranslationService){
        this.translations=TranslationService.all();
    }

    public translate(txt):string {
        Logger.log("ttt:"+txt);
        return this.translations.get(txt);
    }

    public changeToSpanish():void {
        this.t.lang("es");
        this.texto="es";
        this.translations=TranslationService.all();
    }

    public changeToEnglish():void {
        this.t.lang("en");
        this.texto="en";
        this.translations=TranslationService.all();
    }   
}