import { Component,Input } from '@angular/core';

// webpack html imports
let template = require('./cemex-simple-card.component.html');

@Component({
  selector: 'cemex-simple-card',
  template: template,
  styleUrls: ['./cemex-simple-card.component.scss']
})
export class CemexSimpleCardComponent {
  @Input() title: string;
}