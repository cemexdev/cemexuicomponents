import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CemexButtonComponent, ButtonSize, ButtonType } from '../cemex-button/cemex-button.component'

@Component({
    selector: 'cemex-modal',
    template: `<!--Cemex Modal Button-->
    <div [innerHtml]="buttontag" data-toggle="modal" data-target="#myModal"></div>

    <!--JavaScript for Modal with Bootstrap-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
      <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content" [style.top]="top" [style.bottom]="bottom" [style.left]="left" [style.right]="right">
            <div>
                <button type="button" class="close" style="padding: 15px;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="container" style="width:100%">
                <div [innerHtml]="body" class="modal-body">
            </div>
          </div>
        </div>
      </div>
    </div>`,
    styleUrls: ['./cemex-modal.component.css']
})
export class CemexModalComponent {
    @Input() buttontag: any;
    @Input() body: any;
    @Input() top: string;
    @Input() bottom: string;
    @Input() right: string;
    @Input() left: string;

    constructor() {
    this.buttontag = "Tag"
    this.body = "Content"
    this.top = "0"
    this.bottom = "0"
    this.right = "0"
    this.left = "0"
  }
}
