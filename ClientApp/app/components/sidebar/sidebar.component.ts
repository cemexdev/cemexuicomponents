// Garbage
import { Component } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, Request, RequestOptions } from '@angular/http';
import { CredentialsConstants } from '../../constants/credentials.constants';
import { PathConstants } from '../../constants/path.constants';
import { Injectable }     from '@angular/core';
import { ApiService } from '../../services/http.api.service';

// Models
import JobsiteDTO from '../../DTO/jobsiteDTO';
import CustomerDTO from '../../DTO/customerDTO';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    private mainDropOpened: boolean = false;
    jobsites: JobsiteDTO[] = new Array();
    customers: CustomerDTO[] = new Array();

    constructor(private api: ApiService) {
        this.getJobsites();
        this.getCustomers();
    }

    // Delete this after testing:
    reset() {
        console.log("reset");
        this.jobsites = [];
        this.customers = [];
        this.getJobsites();
        this.getCustomers();
    }

    // Controller logic
    // ============================================================
    private getCustomers() {
        this.api.get('qa/v1/secm/customers').subscribe(result => {
            this.customers = result.json()["customers"] as CustomerDTO[];
            //this.jobsitesFromCustomers();
        });
    }

    private jobsitesFromCustomers() {
        for (let customer of this.customers) {
            for (let jobsite of customer.jobsites) {
                this.jobsites.push(jobsite);
            }
        }
    }

    private getJobsites() {
        this.api.get('qa/v2/dm/jobsites/summary?dateFrom=2016-12-01 00:00&dateTo=2016-12-31 23:59').subscribe(result => {
            this.jobsites = result.json()["jobsites"] as JobsiteDTO[];
        });
    }

    // View logic
    // ============================================================
    private toogleMainDropdown() {
        this.mainDropOpened = !this.mainDropOpened;
    }

    private confirmMainDropdown() {
        this.mainDropOpened = false;
    }

    private mainDropdownStyle() {
        let style = "dropdown";
        if (this.mainDropOpened) {
            style += " open";
        }
        return style;
    }
}
