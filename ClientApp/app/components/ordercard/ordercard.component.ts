import { Component, Injectable, NgZone } from '@angular/core';


export class Card {
  orderNum: string;
  deliveries: string;
  orderDesc: string;
  quantity: number;
  notTicketed: number;
  batching: number;
  onSite: number;
  pouring: number;
  finished: number;
  status: number;
  statusTag: string;
  startDate: string;
  delivered: number;

  constructor(orderNum: string, deliveries: string, orderDesc: string, quantity: number, notTicketed: number, batching: number, onSite: number,
    pouring: number, finished: number, status: number, statusTag: string, startDate: string, delivered: number) {
  }
}


// webpack html imports
let template = require('./ordercard.component.html');

@Component({
  selector: 'order-card',
  template: template,
  styleUrls: ['./ordercards.scss']
})

@Injectable()
export class orderCardComponent {
  card1: any;
  card2: any;
  cards: any;
  newCards: any;

  constructor() {
    this.getInfo()
    this.getCards()

  }


  public getInfo() {
    this.cards = [{

      orderNum: '9287391837981',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 25%',
      quantity: 60,
      notTicketed: 20,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 20,
      status: 2,
      statusTag: 'A',
      startDate: 'today',
      delivered: '10'

    },
    {

      orderNum: '9287391837981',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 35%',
      quantity: 60,
      notTicketed: 10,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 30,
      status: 2,
      statusTag: 'B',
      startDate: 'today',
      delivered: '10'

    },
    {

      orderNum: '9287391837988',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'C',
      startDate: 'today',
      delivered: '10'

    },
    {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    },
    {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    },
    {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    }, {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    }, {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    }, {

      orderNum: '9287391837989',
      deliveries: '20',
      orderDesc: 'RC-752-C+F 15%',
      quantity: 60,
      notTicketed: 50,
      batching: 20,
      onSite: 10,
      pouring: 20,
      finished: 50,
      status: 2,
      statusTag: 'D',
      startDate: 'today',
      delivered: '10'

    }];
    // this.card1 = new Card('938424288923', '20', 'RC-752-C+F 15%', 60, 20, 30, 10, 1, 2, 3, 'Pick Up', 'today', 20);
    // console.log(this.card1);
    // return this.card1
  }

  public getCards() {
    this.newCards = [{
      "Orders": [{
        "orderStatusGroupCode": "CMP",
        "orderId": 187050,
        "orderCode": "8005044287",
        "orderType": "1",
        "countryCode": "MX ",
        "salesareaid": 1,
        "customerId": 2336,
        "jobsiteId": 7575,
        "orderTypeId": 132,
        "orderStatusId": "40",
        "orderStatusDesc": "Completed",
        "shippingConditionId": 1,
        "orderReferenceCode": "test",
        "orderReferenceComp": "test",
        "requestedOrderDateTime": "2016-12-24T16:30:00",
        "createdOrderDateTime": "2016-12-23T17:56:03",
        "updatedOrderDateTime": "2016-12-24T15:36:36",
        "programmedOrderDateTime": "2016-12-24T16:51:00",
        "statusOrderDateTime": "2016-12-24T18:38:41",
        "actualOrderDateTime": "2016-12-24T18:38:41",
        "spacingNum": 20,
        "rateNum": 0,
        "spacingDesc": " ",
        "instructionsDesc": "REC CTE SAMUEL VILLANUEVA TEL: 8122008680Acceso por el porton naranja. barda medianera",
        "purchaseOrder": "4500030924",
        "referenceDocumentTypeCode": null,
        "referenceDocumentCode": "0103729226",
        "estimatedTimeMinutesNum": 0,
        "routeCode": null,
        "routeTime": "0",
        "actualTimeMinutesNum": 0,
        "startOrderDateTime": "2016-12-24T16:51:00",
        "endOrderDateTime": "2016-12-24T16:51:00",
        "orderBy": "LN JOEL GLORIA TEL: 0448120402486",
        "isSmallOrder": true,
        "orderPointOfDeliveryId": 10451,
        "pointOfDeliveryCode": "0065466441",
        "pointOfDeliveryDesc": "VALLE AZUL (RARS)",
        "targetRate": 0,
        "isReadyMix": true,
        "pointOfDelivery": {
          "orderPointOfDeliveryId": null,
          "pointOfDeliveryCode": null,
          "pointOfDeliveryDesc": null,
          "addressItem": {
            "addressId": 107037,
            "addressCode": "4000152420",
            "streetCode": null,
            "streetName": "5600 W COMMERCE ST",
            "domicileNum": "",
            "domicileComp": null,
            "settlementCode": null,
            "settlementDesc": "HILLSBOROUGH",
            "postalCode": "33616-1930",
            "referenceStreetDesc": null,
            "referenceStreetComplDesc": null,
            "cityCode": "",
            "cityDesc": "TAMPA",
            "conurbationCode": null,
            "conurbationDesc": "",
            "regionCode": "FL",
            "regionDesc": "Florida",
            "zoneCode": null,
            "zoneDesc": null,
            "countryCode": "US ",
            "timeZone": "UTC-5",
            "latitude": "0.000000000000",
            "longitude": "0.000000000000",
            "altitude": "0.000"
          }
        },
        "orderItem": [
          {
            "orderDetStatusGroupCode": "CNL",
            "orderDetId": 1139688,
            "referenceDocumentHdrTypeCode": null,
            "referenceDocumentHdrCode": "0103729226",
            "referenceDocumentItemCode": "000100",
            "itemSeeqNum": 1001,
            "higherLavelItemSeqNum": 1000,
            "productTypeCode": "ZTEC",
            "productTypeDesc": null,
            "productCode": "000000000010003994",
            "productDesc": "1-150-2-C-28-12-0-1-000",
            "productQuantity": 7,
            "unitId": "102",
            "unitDesc": "M3",
            "plantId": "241",
            "orderDetStatus": "Confirmed",
            "statusOrderDetDateTime": "2016-12-24T18:38:41",
            "programmedOrderDetDateTime": "2016-12-24T16:51:00",
            "loadNumber": "1",
            "truckDesc": null,
            "ETANum": 0,
            "element": "CONCRETO CICLOPEO",
            "slump": "12      ",
            "isReadyMix": true,
            "ticketGroupBy": [
              {
                "ticketId": 342629,
                "truckId": "1",
                "deliveryDateTime": "2016-12-24T16:51:00",
                "ticketStatus": "Finished",
                "isDelivered": "True",
                "deliveredQuantity": 7,
                "ticketStatusDateTime": "2016-12-24T18:38:41",
                "ticketStatusGroupCode": "FIN"
              }
            ]

          }
        ]
      }
      ]
    }];
    console.log(this.newCards);
  }

}


