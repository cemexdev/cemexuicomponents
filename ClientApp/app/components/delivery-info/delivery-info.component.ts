import { Component } from '@angular/core';
import { PathConstants } from '../../constants/path.constants';
import { ApiService } from '../../services/http.api.service';

import OrderDTO from '../../DTO/orderDTO';

@Component({
    selector: 'delivery-info',
    templateUrl: './delivery-info.component.html',
    styleUrls: ['./delivery-info.component.scss']
})
export class DeliveryInfoComponent {
    order: OrderDTO;
    purchaseOrder: string;

    constructor(api: ApiService) {
        api.get('qa/v2/sm/summary/jobsites/2/orders').subscribe(result => {
            this.order = result.json()["Orders"][0] as OrderDTO;
            this.fillOrderInfo();
        });
    }

    fillOrderInfo() {
        this.purchaseOrder = this.order.purchaseOrder;
    }
}
