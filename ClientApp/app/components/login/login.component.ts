import {Component} from '@angular/core';
import {SessionService} from '../../services/session.service';
import {HttpCemex} from '../../services/http.service';
import {CredentialsConstants} from '../../constants/credentials.constants';
import {Broadcaster} from '../../events/broadcaster.event';
import {Logger} from '../../services/logger.service';
import {TranslationService} from '../../services/translation.service';
import {Router} from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [SessionService, HttpCemex, Broadcaster, TranslationService],
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    appKey = "f624df7f-9b35-41c0-8a41-4854b86c84f1";
    appName = "Foreman_App";
    username = "pedro.ferriz@hotmail.com";
    password = "Cemex2016";
    loginError = false;

    constructor(private authenticationService: SessionService,
        private eventDispatcher: Broadcaster,
        private t: TranslationService,
        private router: Router) {}

    login() {
        this.logout();
        CredentialsConstants.setAppCode(this.appName);
        CredentialsConstants.setKeys(this.appKey, this.appKey, "");
        this.authenticationService.startSession(this.username, this.password);
        this.eventDispatcher.on<string>(SessionService.LOGIN_FAIL_EVENT)
            .subscribe(message => {
                this.loginError = true;
                Logger.log('user was logged in');
            });
        this.eventDispatcher.on<string>(SessionService.LOGIN_SUCCESS_EVENT)
            .subscribe(message => {
                this.router.navigate(['home']);
                Logger.log('user was logged out');
            });
    }

    logout(): void {
        this.authenticationService.endSession();
    }
}