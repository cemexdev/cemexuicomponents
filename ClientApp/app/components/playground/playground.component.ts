import { Component } from '@angular/core';
import { CemexButtonComponent, ButtonSize, ButtonType } from '../cemex-button/cemex-button.component';
import { CemexLoaderComponent } from '../cemex-loader/cemex-loader.component';
import { FilterComponent } from '../filter/filter.component';

@Component({
    selector: 'home',
    templateUrl: './playground.component.html',
    styleUrls: ['./playground.component.css']
})
export class PlaygroundComponent {
    // Button types
    buttonPrimary: ButtonType = ButtonType.primary;
    buttonSecondary: ButtonType = ButtonType.secondary;
    buttonDisabled: ButtonType = ButtonType.disabled;

    // Button sizes
    buttonNormal: ButtonSize = ButtonSize.normal;
    buttonMedium: ButtonSize = ButtonSize.medium;
    buttonSmall: ButtonSize = ButtonSize.small;

    // Helpers:
    cemexButtonHelper = `<cemex-button title="Change Plants" [type]=buttonPrimary></cemex-button>
    @Inputs:
      title::string
      [type]::ButtonType
      [size]::ButonType`
    cemexTableHelper = `<cemex-table></cemex-table>`
    cemexHeaderHelper = `<cemex-header></cemex-header>`
    sideBarSelector = `<sidebar></sidebar>`
}
