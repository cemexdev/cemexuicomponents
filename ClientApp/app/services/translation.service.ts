import {CredentialsConstants} from '../constants/credentials.constants';
import {HttpCemex} from './http.service';
import {PathConstants} from '../constants/path.constants';
import {Logger} from './logger.service';
import {Injectable} from '@angular/core';

@Injectable()
export class TranslationService{

    public static translation:Map<string,string>=new Map<string,string>();
    constructor(private http:HttpCemex) {
        this.http.get("/vendor/locale-en.json")
            .toPromise()
            .then(response => this.populateTranslation(response.json()))
            .catch(this.handleError);
    }
    public lang(lang:string):void {
        this.http.get("/vendor/locale-"+lang+".json")
            .toPromise()
            .then(response => this.populateTranslation(response.json()))
            .catch(this.handleError);
    }
    public file(url:string):void {
        this.http.get(url)
            .toPromise()
            .then(response => this.populateTranslation(response.json()))
            .catch(this.handleError);
    }
    private populateTranslation(result) {
        for(let item in result) {
            TranslationService.translation.set(item,result[item]);
        }
    }
    private handleError(error: any): Promise<any> {
        Logger.error(error);
        return Promise.reject(error.message || error);
    }

    /**
     * st comes from static-translation
     */
    public static st(textID:string):string {
        if(this.translation.get(textID)==null || this.translation.get(textID)==undefined){ 
            return "NOT:"+textID;
        } 
        return this.translation.get(textID);
    }

    /**
     * pt comes from public-translation
     */
    public pt(textID:string):string {
        if(TranslationService.translation.get(textID)==null || TranslationService.translation.get(textID)==undefined) {
            return "NOT:"+textID;
        } 
        return TranslationService.translation.get(textID);
    }
    /**
     * gets all translations
     */
    public static all():Map<string,string> {
        return this.translation;
    }
}