import { Injectable } from '@angular/core';
import {HttpModule, RequestOptions, Headers} from '@angular/http';
import { UserDTO } from '../DTO/userDTO';
import {PathConstants} from '../constants/path.constants';
import 'rxjs/add/operator/toPromise';
import {CredentialsConstants} from '../constants/credentials.constants';
import {HttpCemex} from './http.service';
import {LoginDTO} from '../DTO/loginDTO';
import {Logger} from '../services/logger.service';
import {Broadcaster} from '../events/broadcaster.event';

@Injectable()
export class SessionService {
    constructor(private http: HttpCemex, private eventDispatcher: Broadcaster) {}

    currentSession: UserDTO;

    static LOGIN_SUCCESS_EVENT="LOGIN_SUCCESS_EVENT";
    static LOGIN_FAIL_EVENT="LOGIN_FAIL_EVENT";
    static LOGIN_LOGOUT_EVENT ="LOGIN_LOGOUT_EVENT";

    isUserAuthenticated(){
        if(UserDTO!== null)
            return true;
        else return false;
        }

    startSession(user: string, password: string): Promise<any> {
        CredentialsConstants.setJWT(undefined);
        CredentialsConstants.setBearer(undefined);

        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(PathConstants.getWorkingPath() + PathConstants.LOGIN_PATH,
                this.generateUrlString(user, password),
                options)
            .toPromise()
            .then(response => this.processIncomingDataFromLogin(response.json() as LoginDTO))
            .catch(this.handleError);
    }

    getSession(): UserDTO {
        return this.currentSession;
    }

    private processIncomingDataFromLogin(data: LoginDTO) {
        CredentialsConstants.setJWT(data.jwt);
        CredentialsConstants.setBearer(data.oauth2.access_token);
        this.getUserInfo();
    }

    private getUserInfo(): void {
        this.http.get(PathConstants.getWorkingPath() + PathConstants.USER_INFO)
            .toPromise()
            .then(response => this.finalStepInSessionProcess(response.json() as UserDTO))
            .catch(this.handleError);
    }

    private finalStepInSessionProcess(user: UserDTO): void {
        this.currentSession = user;
        this.eventDispatcher.broadcast(SessionService.LOGIN_SUCCESS_EVENT);
    }

    endSession(): void {
        this.currentSession = null;
        this.eventDispatcher.broadcast(SessionService.LOGIN_LOGOUT_EVENT);
    }

    private generateUrlString(user: string, password: string): string {
        return "grant_type=password&scope=security&username=" +
            user +
            "&password=" +
            password +
            "&client_id=" +
            CredentialsConstants.getKey();
    }

    private handleError(error: any): Promise<any> {
        console.error("An error occurred", error);
        this.eventDispatcher.broadcast(SessionService.LOGIN_FAIL_EVENT);
        return Promise.reject(error.message || error);
    }
}