import { Injectable } from '@angular/core';
import {HttpModule, RequestOptions,Headers,Http} from '@angular/http';
import {PathConstants} from '../constants/path.constants';
import 'rxjs/add/operator/toPromise';
import JobsiteDTO from '../DTO/jobsiteDTO';


@Injectable()
export class JobsiteService {

    constructor(private http: Http) { }

    getAllJobsites():Promise<JobsiteDTO[]> {
        return this.http.get(PathConstants.getWorkingPath()+PathConstants.PLANT_PATH)
               .toPromise()
               .then(response => response.json().data )
               .catch(this.handleError);
    }

    

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    
}