import { ApiService }                             from './services/http.api.service';
import { ChartsModule }                           from 'ng2-charts/ng2-charts';
import { FormsModule }                            from '@angular/forms';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { NgModule }                               from '@angular/core';
import { NgbModule }                              from '@ng-bootstrap/ng-bootstrap';
import { RouterModule }                           from '@angular/router';
import { UniversalModule }                        from 'angular2-universal';

// Components

import { AppComponent }                 from './components/app/app.component';
import { NavMenuComponent }             from './components/navmenu/navmenu.component';
import { HomeComponent }                from './components/home/home.component';
import { FetchDataComponent }           from './components/fetchdata/fetchdata.component';
import { CounterComponent }             from './components/counter/counter.component';
import { CemexButtonComponent }         from './components/cemex-button/cemex-button.component';
import { LoginComponent }               from './components/login/login.component';
import { TranslationComponent }         from './components/translation/translation.component';
import { PlaygroundComponent }          from './components/playground/playground.component';
import { TableComponent }               from './components/table/table.component';
import { CemexModalComponent }          from './components/cemex-modal/cemex-modal.component';
import { SidebarComponent }             from './components/sidebar/sidebar.component'
import { HeaderComponent }              from './components/header/header.component'
import { BarChartDemoComponent}         from './components/chartbar/chartbar.component'
import { DeliveryInfoComponent }        from './components/delivery-info/delivery-info.component'
import { DeliveryTemplateComponent }    from './components/delivery-template/delivery-template'
import { orderCardComponent }           from './components/ordercard/ordercard.component';
import { CemexLoaderComponent }         from './components/cemex-loader/cemex-loader.component'
import {CemexChartDonutComponent}       from './components/cemex-chart-donut/cemex-chart-donut.component'
import { CemexSimpleCardComponent }     from './components/cemex-simple-card/cemex-simple-card.component'
import { SimpleCardExample }            from './components/cemex-simple-card/demo.component'
import { Nv3Component}                  from './components/nv3/nv3.component'
import { Nv3Example }                   from './components/nv3/demo.component'
import { nvD3 }                         from 'ng2-nvd3'
import { CemexHalfDonutComponent }      from './components/cemex-half-donut/cemex-half-donut.component'
import { CemexProgressChartComponent }  from './components/cemex-progress-chart/cemex-progress-chart.component'
import { CemexProgressChartExample }    from './components/cemex-progress-chart/demo.component'
import { ChartGaugeExample }    from './components/cemex-chart-donut/demo.component';





import { FilterComponent }                        from './components/filter/filter.component';


@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        BarChartDemoComponent,
        CemexButtonComponent,
        CemexModalComponent,
        CemexLoaderComponent,
        CounterComponent,
        DeliveryInfoComponent,
        DeliveryTemplateComponent,
        FetchDataComponent,
        FilterComponent,
        HeaderComponent,
        HomeComponent,
        LoginComponent,

        TableComponent,
        CemexChartDonutComponent,
        SidebarComponent,
        HeaderComponent,
        BarChartDemoComponent,
        
        CemexSimpleCardComponent,
        SimpleCardExample,
        nvD3,
        Nv3Component,
        Nv3Example,
        CemexModalComponent,
        SidebarComponent,
        HeaderComponent,
	ChartGaugeExample, 


        BarChartDemoComponent,
        CemexHalfDonutComponent,
        CemexProgressChartComponent,
        CemexProgressChartExample,
        orderCardComponent,
        DeliveryInfoComponent,
        DeliveryTemplateComponent,
        orderCardComponent,
        DeliveryInfoComponent,
        DeliveryTemplateComponent,
        CemexLoaderComponent,


        NavMenuComponent,
        orderCardComponent,
        PlaygroundComponent,
        SidebarComponent,
        TranslationComponent,
        TableComponent
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        ChartsModule,
        NgbModule,
        RouterModule.forRoot([
            { path: '',   redirectTo: 'home', pathMatch: 'full' },
            { path: 'home',       component: HomeComponent },
            { path: 'counter',    component: CounterComponent },
            { path: 'delivery',   component: DeliveryTemplateComponent },
            { path: 'demo-modal', component: CemexModalComponent },
            { path: 'fetch-data', component: FetchDataComponent },

            { path: 'login', component: LoginComponent },
            { path: 'translate', component: TranslationComponent },
            { path: 'donut-chart', component: ChartGaugeExample },
            { path: 'simple-card', component: SimpleCardExample },
            { path: 'playground', component: PlaygroundComponent },
            {path:'nv3',component:Nv3Example},
            { path: 'demo-modal', component: CemexModalComponent },
            { path: 'progress-chart-example', component: CemexProgressChartExample },
            { path: 'delivery', component: DeliveryTemplateComponent },

            { path: 'ordercard',  component: orderCardComponent },
            

            { path: '**', redirectTo: 'home' }
        ]),FormsModule
    ],
    providers: [
        {
            provide: ApiService,
            useFactory: (backend: XHRBackend, options: RequestOptions) => {
                return new ApiService(backend, options);
            },
            deps: [XHRBackend, RequestOptions]
        }
    ],
    entryComponents:[
        BarChartDemoComponent,CemexChartDonutComponent
    ]
})
export class AppModule {
}
